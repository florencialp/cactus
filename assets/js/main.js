//evento botón "crear especie"
let botonCrearEspecie = document.getElementById("btnCrearEspecie");

botonCrearEspecie.addEventListener("click", function(event) {
    event.preventDefault();

    agregarNuevaEspecie();
    listarEspeciesEnTabla();
});

// se crea el objeto Especie
function Especie(nombreComun, nombreCientifico, origen) {
    this.nombreComun = nombreComun;
    this.nombreCientifico = nombreCientifico;
    this.origen = origen;
}

// se inicializan diferentes especies
let cactus1 = new Especie("Acanthocalycium spp", "Acanthocalycium", "Argentina");
let cactus2 = new Especie("Aporocactus flagelliformis", "Aporocactus", "México");
let cactus3 = new Especie("Ariocarpus spp", "Ariocarpus", "México");

// se crea un array que contendrá todas las especies
let especies = [cactus1, cactus2, cactus3];


let agregarNuevaEspecie = function() {
    let nombreComun = document.getElementById("nombreComun").value;
    let nombreCientifico = document.getElementById("nombreCientifico").value;
    let origen = document.getElementById("origen").value;

    if (nombreComun != "" && nombreCientifico != "" && origen != "") {
        let cactusNuevo = new Especie(nombreComun, nombreCientifico, origen);
        especies.push(cactusNuevo);
    }
}


let listarEspeciesEnTabla = function() {
    let table = document.getElementById("table");
    table.classList.remove("hide");

    let formBody = document.getElementById("formBody");
    formBody.innerHTML = "";

    for (let i = 0; i < especies.length; i++) {

        let tr = document.createElement("tr");
        let th = document.createElement("th");

        th.appendChild(document.createTextNode(i + 1));
        tr.appendChild(th);
        formBody.appendChild(tr);

        let tdNombre = document.createElement("td");
        tdNombre.appendChild(document.createTextNode(especies[i].nombreComun));
        tr.appendChild(tdNombre);

        let tdCientifico = document.createElement("td");
        tdCientifico.appendChild(document.createTextNode(especies[i].nombreCientifico));
        tr.appendChild(tdCientifico);

        let tdOrigen = document.createElement("td");
        tdOrigen.appendChild(document.createTextNode(especies[i].origen));
        tr.appendChild(tdOrigen);
    }
}