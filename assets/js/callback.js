function sum(num1, num2) {
    return num1 + num2;
}

function calc(num1, num2, callback) {

    return callback(num1, num2);
}

console.log(sum(5, 15));

calc(2, 2, sum);

function resta(num1, num2) {
    return num1 - num2;
}

function loadDoc(url, cFunction) {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            cFunction(this);
        }
    };

    xhttp.open("GET", url, true);
    xhttp.send();
}

function myFunction1(xhttp) {

}

function myFunction2(xhttp) {

}

//loadDoc("url", myFunction1);
//loadDoc("url", myFunction2);


var character;

function getEpisodes(url) {
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            character = this.responseText;
            console.log(character);
        }
    }
    xhttp.open("GET", url, true);
    xhttp.send();

}